# React coding challenge

Hey! :wave: You're probably looking to join the Partipost team. If so, this repo
has a brief for a React app - follow the instructions below and we'll be in
touch!

### Specs

We've received some feedback from our brands: they'd like a way to invite
Partiposters to a campaign. We've got our [mockup](invite-mockup.gif) and
functional specs for a basic SPA:

![React challenge mockup](invite-mockup.gif)

- When the user loads the page, call our API to fetch a random list of 5
  Partiposters and their details, including their fees.
- While the data is being fetched, show a spinner.
- Users can add or remove Partiposters from their invite list.
- The count and total fees (in the top right corner) should be updated after
  every change in selection to reflect the current invites.
- Clicking on the 'EMPTY CART' button clears all invites.

Our API isn't quite ready, so you'll have to mock it for development, including
simulating a 1s response time. We expect the successful response will look like:

```
HTTP/1.1 200 OK
{
  "partiposters": [
    {
      "id": 3,
      "image_url": "https://partipost.cdn/images/users/3.png",
      "name": "Jane Doe",
      "user_name": "janedoe.partipost",
      "fee": 25
    },
    { ... },
    { ... },
    { ... },
    { ... }
  ]
}

```

How you choose to mock the API is up to you -`db.json` is provided for
reference.

**Design assets**

- Navy blue hex code: `#253778`
- Light blue hex code: `#5eb2ed`
- Shopping cart icon: [./cart.svg](cart.svg)

### Guidelines

- Your app should support IE11+ and the latest versions of Chrome, Firefox, and
  Safari.
- Feel free to use any third party JS and CSS libraries as you see fit.
- Prioritise quality over completeness. It's absolutely okay to skip features
  you don't have time for as long as you clearly mention them in your `README`
  file.
- Feel free to make changes to the UI if you have any suggestions

We look out for the following things when reviewing your code:

- **Designing to spec**: does the app follow the design and functional specs?
  Are missing features explicitly mentioned?
- **Code cleanliness**: is the code production ready - maintainable and easy to
  understand?
- **Version control**: do you know when to commit? Are the commit messages clear
  enough to communicate changes to rest of the team?

### Next steps

1. Share your code as a git repository. Either create a private repository on
   your platform of choice (e.g. Gitlab, Github, Bitbucket) and give
   `tech@partipost.com` access, or email `tech@` with your directory zipped
   (including `.git/`).
2. We'll be in touch to schedule a follow up meeting to discuss how you got on.
